Pyngwin
====

The last desktop window manager you'll (hopefully) ever need.

Objectives
----------

Pyngwin aims to be a batteries-included desktop window manager (DWM) for Windows 10.
The goal is to replace the built-in DWM that comes with Windows 10 for reasons that may not be obvious.

The amount of control you're given is embarrassingly limited. Imagine that you're like me: I commonly find myself working in a number of workspaces. These workspaces are typically very specific to some very limited tasks, i.e. a workspace for coding, another for Googling, and another for something like Discord (or what-have-you). The default Windows' 10 Virtual Desktops are a decent start, but Pyngwin aims to do better.

About
-----
Pyngwin might be considered the [i3](https://github.com/i3/i3) of Win10. While i3 and Pyngwin may be similar, Pyngwin tries to do things just a little bit better. Specifically, customizing Pyngwin. The developer(s) of Pyngwin found i3 configs to be (just a bit) convoluted. Many configuration choices can easily be simplified in such a way that you as the user maintain the same amount of control while still being able to easily wrap your head around what's actually going to happen. Appearance is everything, and so, while i3 may allow you maximum control over your machine (of course, this is speaking from a Linux point of view), it comes at a great cost (and we're striving for a sexy "box" all the way down to the config files).

What can I use it for?
----------------------
* Pyngwin provides a snappy "bar" to replace the ever-so in the way Windows taskbar
    - Which can be customized to your hearts content!
* Create, customize, and control "workspaces" for your organization pleasure
* Seriously take control of your machine. Hotkeys for apps, workspace goodness, etc. Make your machine work exactly how you want

To-Do
-----
* Pyngwin-bar
    - A bar (like i3-bar) that stays on top of all windows, showing the user which workspace they're in
    - Also shows local time, network status, etc. in a configurable manner
    - PyQt5 or PySide2
* Override/disable Win10s virtual desktops feature. We'll need full control of behavior.
* Hotkeys for WM control
    - [keyboard](https://github.com/boppreh/keyboard)
* Option to choose between tiling and floating window modes

Ideas
-----
* Pyngwin-explorer
    
    Wouldn't it be nice to be able to customize and control the file explorer without having to find and download a suitable software capable of this? I think so, too.


Contributing
------------
The purpose of Pyngwin (to be the ultimate Win10 window manager) is a huge goal to say the least, and the [Windows API is an 800 pound monster covered with hair](https://stackoverflow.com/a/342740). Documentation for interacting with win32api is either poorly written, convoluted, or completely missing. Some intended functionality within Pyngwin might be the same. Development can be slow going: especially development regarding wrapping Windows features in Python in such a way that a Linux patch can be applied in the backend for use on *nix systems.

With the above in mind, if you would like to submit a pull request, have at it! However, please do provide any applicable reading material and tests; as well as commenting your pull request as best as you can so that future changes can be made constructively, and those that want to make these future changes don't have to scramble to find the same information that you found (if it even still exists).
