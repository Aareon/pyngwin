import ctypes

GetSystemMetrics = ctypes.windll.user32.GetSystemMetrics


def get_screen_resolution():
    return (GetSystemMetrics(0), GetSystemMetrics(1))
