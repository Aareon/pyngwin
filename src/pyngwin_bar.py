import sys

import gevent
from PyQt5 import QtCore
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QWidget


class PyngwinBar(QWidget):
    def __init__(self, app):
        super().__init__()

        self.app = app
        self.screen_resolution = app.desktop().screenGeometry()

        self.initUI()

    def initUI(self):
        # create the bar and set flags
        self.setGeometry(0, 0, self.screen_resolution.width(), 27)
        self.setWindowFlags(
            QtCore.Qt.FramelessWindowHint
            | QtCore.Qt.WindowStaysOnTopHint
            | QtCore.Qt.Tool  # no taskbar icon
        )

        # set bar background color
        self.set_color("#263238", background=True)

    def init_timer(self):
        # create time label and move it into position
        self.time_label = QLabel("", self)
        self.time_label.move(
            (self.screen_resolution.width() - self.time_label.width()) + 10,
            (self.height() / 4),
        )

        # set time label font color
        self.set_color("#c3e88d", foreground=True)

        # start clock
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.show_time)
        self.timer.start()

    def show_time(self):
        self.time_label.setText(QtCore.QTime.currentTime().toString())
        self.time_label.adjustSize()

    def init_workspace(self, name, stylesheet=None):
        ws_button = QPushButton(name, self)
        if stylesheet is not None:
            ws_button.setStyleSheet(stylesheet)
        else:
            ws_button.setStyleSheet(
                """
            QPushButton {
                padding: 2px 8px 2px 8px;
                border: 2px solid #193b42;
                /* border-radius: 1px; */
                font: normal 16px/normal "Lucida Console", Monaco, monospace;
                color: #c3e88d;
            }
            QPushButton:pressed {
                background: #1a2327;
                color: #bd92df;
            }
            """
            )
        ws_button.move(0, self.height() - ws_button.height() + 3)

    def set_color(self, color_code, foreground=False, background=False):
        if foreground is False and background is False:
            raise Exception(
                "set_widget_color: keyword argument"
                " foreground or background must equal True"
            )
        elif foreground is True and background is True:
            raise Exception(
                "set_widget_color: only one keyword argument "
                " (foreground, backgroud) must equal True"
            )

        p = self.palette()
        color = QColor()
        color.setNamedColor(color_code)
        if foreground:
            p.setColor(self.foregroundRole(), color)
        elif background:
            p.setColor(self.backgroundRole(), color)

        self.setPalette(p)
        return self


def main_loop(app):
    while True:
        app.processEvents()
        while app.hasPendingEvents():
            app.processEvents()
            gevent.sleep(0.05)
        gevent.sleep(0.1)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    bar = PyngwinBar(app)

    bar.init_timer()
    bar.init_workspace("1: Coding")
    bar.show()

    gevent.joinall([gevent.spawn(main_loop, app)])

    # sys.exit(app.exec_())
