import os
import sys

import toml
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QColor


class PyngwinBar(QWidget):
    def __init__(self, app, config=None):
        super().__init__()

        self.app = app
        self.screen_resolution = self.app.desktop().screenGeometry()
        self.config = config
        self.screen_w = (
            config.get("bar")
            .get("width")
            .format(screen_width=self.screen_resolution.width())
        )
        self.screen_h = (
            config.get("bar")
            .get("height")
            .format(screen_height=self.screen_resolution.height())
        )
        self.config = config
        self.initUI()

    def initUI(self):
        self.setGeometry(0, 0, self.screen_resolution.width(), 26)
        self.setWindowFlags(
            QtCore.Qt.FramelessWindowHint
            | QtCore.Qt.WindowStaysOnTopHint
            | QtCore.Qt.Tool  # no taskbar icon
        )

        # set bar background color
        self.setAutoFillBackground(True)
        palette = self.palette()

        bar_conf = self.config.get("bar")

        if bar_conf is not None:
            bar_bg_color = bar_conf.get("bg-color")

            if bar_bg_color is not None:
                color = QColor()
                color.setNamedColor(bar_bg_color)
                palette.setColor(self.backgroundRole(), color)
                self.setPalette(palette)

        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    with open(
        f"{os.path.dirname(os.path.realpath(__file__))}/../config.toml", "r"
    ) as f:
        config = toml.load(f)

    window = PyngwinBar(app, config=config)
    sys.exit(app.exec_())
